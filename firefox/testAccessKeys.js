/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Mozmill Test Code.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Adrian Kalla <akalla@aviary.pl>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var RELATIVE_ROOT = '../shared-modules';
var MODULE_REQUIRES = ['CommonA11yL10nAPI', 'L10nAPI', 'ModalDialogAPI', 'TabbedBrowsingAPI'];

const gDelay = 1900;
const gTimeout = 5000;

var jumlib = {}; 
Components.utils.import("resource://mozmill/modules/jum.js", jumlib);

var setupModule = function(module) {
  controller = mozmill.getBrowserController();
  domWalker = new CommonA11yL10nAPI.DOMWalker(controller);
  l10n = new L10nAPI.l10n(controller);
}

var teardownModule = function(module) {
 ;
}

var testNew = function() {
  var doc = controller.window.document;
  var root = doc.documentElement;
  var node = doc.getElementById('main-menubar');
  l10n.checkAccesskeysMain(controller, doc, node, {} )
}

var aaaaaAccesskeys = function() {
  TabbedBrowsingAPI.closeAllTabs(controller);
  var success;
  
  success = controller.keypress(null, "n", {ctrlKey: mozmill.isMac, altKey: !mozmill.isMac});
  controller.sleep(300);
  //controller.window.alert(success+"\n");
  success = controller.keypress(null, "d", {ctrlKey: mozmill.isMac, altKey: !mozmill.isMac});
  controller.sleep(500);
  var bla = controller.window.document.getElementsByAttribute('_moz-menuactive', 'true');
  var bla2 = "";
  //bla=bla._moz-menuactive+'\n\n\n\n';
  for (i=0; i < bla.length; i++) {
    bla2+=bla[i].id+"\n";
  }
  bla2+="\n\n";
  
  if (bla[0].nodeName == "menu" && bla[0].hidden != true && bla[0].disabled != true && bla[0].open && bla[0].open == true) {
    
  }
  
  if (bla[0].nodeName == "menuitem" && bla[0].hidden != true && bla[0].disabled != true) {
    
  }
  //controller.assertJS('subject._moz-menuactive==true' , bla);
  //controller.assertJSProperty(new elementslib.ID(controller.window.document, 'menu_openAddons'), '_moz-menuactive');
  
  controller.sleep(500);
  //controller.window.alert(success+"\n");
  success = controller.keypress(null, "d", {ctrlKey: mozmill.isMac, altKey: !mozmill.isMac});
  controller.sleep(500);
  var bla = controller.window.document.getElementsByAttribute('_moz-menuactive', 'true');
  //bla=bla._moz-menuactive+'\n\n\n\n';
  for (i=0; i < bla.length; i++) {
    bla2+=bla[i].id+"\n";
  }
  controller.window.alert(bla2);
}
