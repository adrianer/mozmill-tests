/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Mozmill Test Code.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Adrian Kalla <akalla@aviary.pl>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var RELATIVE_ROOT = '../shared-modules';
var MODULE_REQUIRES = ['CommonA11yL10nAPI', 'L10nAPI', 'PrefsAPI'];

const gDelay = 1900;
const gTimeout = 5000;

var jumlib = {}; 
Components.utils.import("resource://mozmill/modules/jum.js", jumlib);

var setupModule = function(module) {
  controller = mozmill.getPreferencesController();
  domWalker = new CommonA11yL10nAPI.DOMWalker(controller);
  l10n = new L10nAPI.l10n(controller);
  //prefDialog = new PrefsAPI.preferencesDialog(controller);
}

var teardownModule = function(module) {
  PrefsAPI.openPreferencesDialog(prefPaneReset);
}

var prefPaneReset = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);

  prefDialog.paneId = 'paneMain';
  prefDialog.close();
}


var prefPaneAdvanced = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  controller.sleep(gDelay);
  prefDialog.paneId = 'paneAdvanced';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}

var prefPaneSecurity = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  controller.sleep(gDelay);
  prefDialog.paneId = 'paneSecurity';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}

var prefPanePrivacy = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  
  prefDialog.paneId = 'panePrivacy';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}

var prefPaneApplications = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  controller.sleep(gDelay);
  prefDialog.paneId = 'paneApplications';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}

var prefPaneContent = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  controller.sleep(gDelay);
  prefDialog.paneId = 'paneContent';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}

var prefPaneTabs = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  controller.sleep(gDelay);
  prefDialog.paneId = 'paneTabs';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}

var prefPaneMain = function(controller)
{
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  var doc = controller.window.document;
  var root = doc.documentElement;
  controller.sleep(gDelay);
  prefDialog.paneId = 'paneMain';
  controller.sleep(gDelay);
  
  domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  prefDialog.close();
}


// This test repeats every error on every following tab :/
var prefPaneCroppedElements = function(controller) {
  let prefDialog = new PrefsAPI.preferencesDialog(controller);
  
  var doc = controller.window.document;
  var root = doc.documentElement;
  
  // List of all available panes inside the Preferences window
  var panes = [
               "paneMain", "paneTabs", "paneContent", "paneApplications",
               "panePrivacy", "paneSecurity", "paneAdvanced", "paneMain"
              ];

  // Step through each of the panes
  for each (pane in panes) {
    prefDialog.paneId = pane;
    var node = doc.getElementById(pane);
    //controller.waitForEval('subject.contentHeight > 0', gDelay, 100, node);
    controller.sleep(gDelay);
  
    domWalker.getNodes_recursion_main(doc, root, l10n.filterNodes, postFunctionCallback2, postFunctionCallback);
  }
  prefDialog.close();
}

var testPrefPaneCroppedElements = function() {
  PrefsAPI.openPreferencesDialog(prefPaneCroppedElements);
}


//
//
//// Just the first test reports errors correctly (the first to run, it does not matter which one exactly!).
//// All following ones do not report anything :/
//var testPrefPaneMain = function() {
//  PrefsAPI.openPreferencesDialog(prefPaneMain);
//}
//
//var testPrefPaneTabs = function() {
//  PrefsAPI.openPreferencesDialog(prefPaneTabs);
//}
//
//var testPrefPaneContent = function() {
//  PrefsAPI.openPreferencesDialog(prefPaneContent);
//}
//
//var testPrefPanePrivacy = function() {
//  PrefsAPI.openPreferencesDialog(prefPanePrivacy);
//}
//
//var testPrefPaneSecurity = function() {
//  PrefsAPI.openPreferencesDialog(prefPaneSecurity);
//}
//
//var testPrefPaneAdvanced = function() {
//  PrefsAPI.openPreferencesDialog(prefPaneAdvanced);
//}
//
//var testPrefPaneApplications = function() {
//  PrefsAPI.openPreferencesDialog(prefPaneApplications);
//}

var postFunctionCallback = function(badBoxes, doc) {
  //var badBoxes = [];
  //for (elem in nodeList) {
  //  if (elem.boxObject) {
  //    badBoxes = badBoxes.concat(l10n.checkDimensions(controller, elem.boxObject));
  //  }
  //}

  jumlib.assertEquals(badBoxes.length, 0, 'cropped boxes '+badBoxes);
  if (badBoxes != undefined && badBoxes != 'undefined' && badBoxes != null && badBoxes.length > 0) {
    l10n.createScreenshot(badBoxes, doc);
  }
}

var postFunctionCallback2 = function(elem) {

    if (elem.boxObject) {
      return l10n.checkDimensions(controller, elem);
    } else {
      return [];
    }
}