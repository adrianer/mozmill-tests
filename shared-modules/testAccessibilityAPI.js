/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is MozMill Test code.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Marco Zehe <mzehe@mozilla.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

/**
 * @fileoverview
 * The AccessibilityAPI adds support for accessibility related functions. It also gives
 * access to testing UI elements.
 *
 * @version 1.0.0
 */

var MODULE_NAME = 'AccessibilityAPI';

const gTimeout = 5000;

/**
 * Constructor
 */
function accessibility()
{
  this._controller = null;
  
  this._ar = Cc["@mozilla.org/accessibleRetrieval;1"]
                .getService(Ci.nsIAccessibleRetrieval);
}

/**
 * Accessibility class
 */
accessibility.prototype = {
  get controller() { return this._controller; },

  /**
   * Returns the accessible for the given element.
   *
   * @param elm  The element for which an accessible is to be returned
   * @param interfaces  the interfaces (if any) that should be queried.
   */
  getAccessible : function accessibility_getAccessible(elm, interfaces) {
    var acc = null;
    try {
      acc = this._ar.getAccessibleFor(elm);
    } catch(ex) {}

    if (!acc)
      return null;

    if (!interfaces)
      return acc;

    if (interfaces instanceof Array) {
      for (var index = 0; index < interfaces.length; index++) {
        try {
          acc.QueryInterface(interfaces[index]);
        } catch (ex) {
          return null;
        }
      }
      return acc;
    }

    try {
      acc.QueryInterface(interfaces);
    } catch (ex) {
      return null;
    }
  
    return acc;
  },

  /**
   * Convert role to human readable string.
   */
  roleToString : function accessibility_roleToString(role)
  {
    return this._ar.getStringRole(role);
  },

  /**
   * Convert states to human readable string.
   */
  statesToString : function accessibility_statesToString(states, extraStates)
  {
    var list = this._ar.getStringStates(states, extraStates);

    var str = "";
    for (var index = 0; index < list.length - 1; index++)
      str += list.item(index) + ", ";

    if (list.length != 0)
      str += list.item(index)

    return str;
  },
};
