/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is MozMill Test code.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Adrian Kalla <akalla@aviary.pl>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var MODULE_NAME = 'CommonA11yL10nAPI';

var RELATIVE_ROOT = '.';
var MODULE_REQUIRES = ['CommonA11yL10nAPI', 'ModalDialogAPI'];

const gTimeout = 5000;

var jumlib = {}; 
Components.utils.import("resource://mozmill/modules/jum.js", jumlib);

/**
 * Constructor
 * 
 * @param {MozMillController} controller
 *        MozMill controller of the browser window to operate on.
 */
function DOMWalker(controller)
{
  this._controller = controller;
}

DOMWalker.prototype = {
  /**
   * Returns the MozMill controller
   *
   * @returns Mozmill controller
   * @type {MozMillController}
   */
  get controller() {
    return this._controller;
  },
  
  getNodes_iterator : function DOMWalker_getNodes_iterator(doc, root, filter, callback1, callback2) {
    if(!(callback1 && callback2))
      throw new Error("No callbacks given");
    
    var nodeIterator = doc.createNodeIterator(root, 1, filter, false); // NodeFilter.SHOW_ELEMENT == 1
    
    var badBoxes = [];  
    //var nodeList = [];
    var currentNode;  

    while ((currentNode = nodeIterator.nextNode())) {  
      //nodeList.push(currentNode);
      controller.sleep(1);
      //if (currentNode.boxObject) {
      var badBox = callback1(currentNode);
      if (badBox != undefined && badBox.length > 0) // Why can it be undefined?! Maybe if the hight is too low, so the element is not being displayed??
        badBoxes = badBoxes.concat(badBox);
      //}
    }
    
    // What for do I have to wait here?? It does not work without waiting for a while...
    //controller.sleep(200);
    
    //callback(nodeList);
    callback2(badBoxes, doc);
  },
  
  // this should NOT work properly, but lets test it anyway:
  getNodes_walker : function DOMWalker_getNodes_walker(doc, root, filter, callback1, callback2) {
    if(!(callback1 && callback2))
      throw new Error("No callbacks given");
    
    var treeWalker = doc.createTreeWalker(root, 1, filter, false); // NodeFilter.SHOW_ELEMENT == 1
    
    var badBoxes = [];  
    //var nodeList = [];
    var currentNode;  
     
    while ((currentNode = treeWalker.nextNode())) {  
      //nodeList.push(currentNode);
      controller.sleep(1);
      //if (currentNode.boxObject) {
      var badBox = callback1(currentNode);
      if (badBox != undefined && badBox.length > 0) // Why can it be undefined?! Maybe if the hight is too low, so the element is not being displayed??
        badBoxes = badBoxes.concat(badBox);
      //}
    }
    
    //callback(nodeList);
    callback2(badBoxes, doc);
  },
  
  getNodes_recursion : function DOMWalker_getNodes_recursion(doc, root, filter, callback1, callback2) {
    if(!(callback1 && callback2))
      throw new Error("No callbacks given");
    
    var nodes;
    var nodesWithoutRejected = [];
    var badBoxes = [];
    
    if (root.hasChildNodes()) {
      nodes = root.childNodes;
      
      for (var i = 0; i < nodes.length; i++) {
        //controller.sleep(1);
        var wanted = filter(nodes[i]);
        
        if (wanted == 1 || wanted == 4) { //controller.window.NodeFilter.FILTER_ACCEPT; // 4 == "conditional accept"
            var badBox = callback1(nodes[i]);
            if (badBox != undefined && badBox.length > 0) // Why can it be undefined?! Maybe if the hight is too low, so the element is not being displayed??
              badBoxes = badBoxes.concat(badBox);
            if (wanted != 4)
              nodesWithoutRejected.push(nodes[i]);
        } else if (wanted == 2) { //controller.window.NodeFilter.FILTER_REJECT; // 2
            ;
        } else if (wanted == 3) { //controller.window.NodeFilter.FILTER_SKIP; // 3
            nodesWithoutRejected.push(nodes[i]);
        } else {
            this._controller.window.dump("else: "+wanted+"\n");
            return new Error("We shouldn't be here...");
        }
      }
      
      for (var i = 0; i < nodesWithoutRejected.length; i++) {
        var badBox = this.getNodes_recursion(doc, nodesWithoutRejected[i], filter, callback1, callback2)
        if (badBox != undefined && badBox.length > 0)
          badBoxes = badBoxes.concat(badBox);
      }
    }
    
    return badBoxes;
  },
  
  getNodes_recursion_main : function DOMWalker_getNodes_recursion_main(doc, root, filter, callback1, callback2) {
    if(!(callback1 && callback2))
      throw new Error("No callbacks given");
    
    var badBoxes = null;
    
    badBoxes = this.getNodes_recursion(doc, root, filter, callback1, callback2);
    
    callback2(badBoxes, doc);
  }
}