/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is MozMill Test code.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Adrian Kalla <akalla@aviary.pl>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var MODULE_NAME = 'L10nAPI';

var RELATIVE_ROOT = '.';
var MODULE_REQUIRES = ['CommonA11yL10nAPI', 'ModalDialogAPI'];

const gTimeout = 5000;

var jumlib = {}; 
Components.utils.import("resource://mozmill/modules/jum.js", jumlib);

/**
 * Constructor
 * 
 * @param {MozMillController} controller
 *        MozMill controller of the browser window to operate on.
 */
function l10n(controller)
{
  this._controller = controller;
}

l10n.prototype = {

  saveCanvas : function l10n_saveCanvas(canvas, destFile) {
    // convert string filepath to an nsIFile
    var file = Components.classes["@mozilla.org/file/local;1"]
                         .createInstance(Components.interfaces.nsILocalFile);
    file.initWithPath(destFile);
    file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0666);
  
    // create a data url from the canvas and then create URIs of the source and targets  
    var io = Components.classes["@mozilla.org/network/io-service;1"]
                       .getService(Components.interfaces.nsIIOService);
    var source = io.newURI(canvas.toDataURL("image/png", ""), "UTF8", null);
    var target = io.newFileURI(file)
      
    // prepare to save the canvas data
    var persist = Components.classes["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
                            .createInstance(Components.interfaces.nsIWebBrowserPersist);
    
    persist.persistFlags = Components.interfaces.nsIWebBrowserPersist.PERSIST_FLAGS_REPLACE_EXISTING_FILES;
    persist.persistFlags |= Components.interfaces.nsIWebBrowserPersist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;
      
    // save the canvas data to the file
    persist.saveURI(source, null, null, null, null, file);
  },
  
  checkDimensions : function l10n_checkDimensions(_cont, child) {
    // Check whether the XUL boxObject has screen coordinates outside of
    // the screen coordinates of its parent. If there's no parent, return.
    // Returns a list of boxes that can be highlighted in a screenshot.
    var childBox = child.boxObject;
    var parent = childBox.parentBox;
    if (!parent || parent == child.element || !parent.boxObject) {
      // toplevel element or hidden stuff like script tags
      return [];
    }
    var parentBox = parent.boxObject;
    
    var badRects = [];
    if (childBox.height && childBox.screenX < parentBox.screenX) {
      badRects.push([childBox.x, childBox.y, parentBox.x - childBox.x, childBox.height]);
      jumlib.assert(false, 'to the left');
    }
    if (childBox.height && childBox.screenX + childBox.width > parentBox.screenX + parentBox.width) {
      badRects.push([parentBox.x + parentBox.width, childBox.y, childBox.x + childBox.width - parentBox.x - parentBox.width, childBox.height]);
      jumlib.assert(false, 'to the right');
      controller.window.dump(childBox.screenX +","+ childBox.width +" <- child | parent -> "+ parentBox.screenX +","+ parentBox.width);
    }
    
    // we don't want to test menupopup's, as they always report the full height of all items in the popup
    if (child.nodeName != 'menupopup' && parent.nodeName != 'menupopup') {
      if (childBox.width && childBox.screenY < parentBox.screenY) {
        badRects.push([childBox.x, childBox.y, parentBox.y - childBox.y, childBox.width]);
        jumlib.assert(false, 'at the top');
      }
      if (childBox.width && childBox.screenY + childBox.height > parentBox.screenY + parentBox.height) {
        badRects.push([childBox.x, parentBox.y + parentBox.height, childBox.width, childBox.y + childBox.height - parentBox.y - parentBox.height]);
        jumlib.assert(false, 'at the bottom');
        controller.window.dump(childBox.screenY +","+ childBox.height +" <- child | parent -> "+ parentBox.screenY +","+ parentBox.height);
      }
    }
    
    return badRects;
  },
  
  createScreenshot : function l10n_createScreenshot(badBoxes, doc) {
    
    var maxWidth = doc.documentElement.boxObject.width;
    var maxHeight = doc.documentElement.boxObject.height;
    var rect = [];
    for (var i=0, ii=badBoxes.length; i<ii; ++i) {
      rect = badBoxes[i];
      //this._controller.window.alert(rect);
      if (rect[0] + rect[2] > maxWidth) maxWidth = rect[0] + rect[2];
      if (rect[1] + rect[3] > maxHeight) maxHeight = rect[1] + rect[3];
    }
    var canvas = doc.createElementNS("http://www.w3.org/1999/xhtml", "canvas");
    var width = doc.documentElement.boxObject.width;
    var height = doc.documentElement.boxObject.height;
    canvas.width = maxWidth;
    canvas.height = maxHeight;
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0,0, canvas.width, canvas.height);
    ctx.save();
    ctx.drawWindow(controller.window, 0, 0, width, height, "rgb(0,0,0)");
    ctx.restore();
    ctx.save();
    ctx.fillStyle = "rgba(255,0,0,0.4)";
    for (var i=0, ii=badBoxes.length; i<ii; ++i) {
      var rect = badBoxes[i];
      jumlib.assert(false, [maxWidth, maxHeight]);
      jumlib.assert(false, rect);
      ctx.fillRect(rect[0], rect[1], rect[2], rect[3]);
    }
    ctx.restore();
    this.saveCanvas(canvas, '/tmp/screenshot.png');
  },
  
  // filter for checking dimensions
  filterNodes : function l10n_filterNodes(aNode)
  { 
    //switch (aNode.localName) {
    switch (aNode.boxObject) {
      case false : return controller.window.NodeFilter.FILTER_SKIP;
      case null : return controller.window.NodeFilter.FILTER_SKIP;
      case undefined : return controller.window.NodeFilter.FILTER_SKIP;
      //case "template": return NodeFilter.FILTER_REJECT; // 2
      //case "radiogroup": return NodeFilter.FILTER_SKIP; // 3
      default: {
        if (aNode.hidden != true) {
          if (aNode.localName=="prefpane" && aNode.selected==false) {
            return controller.window.NodeFilter.FILTER_REJECT;
          } else {
            return controller.window.NodeFilter.FILTER_ACCEPT;
          }
        } else {
          return controller.window.NodeFilter.FILTER_REJECT;
        }
      }
    }
  },
  
  
  
  
  
  
  
  
  
  // filter just for menus for checking accesskeys
  filterNodesAKeysMenus : function l10n_filterNodesAKeys(aNode)
  { 
    switch (aNode.boxObject) {
      case false : return controller.window.NodeFilter.FILTER_SKIP;
      case null : return controller.window.NodeFilter.FILTER_SKIP;
      case undefined : return controller.window.NodeFilter.FILTER_SKIP;
      default: {
        if (aNode.accessKey) {//&& aNode.accesskey.length > 0
          if (aNode.nodeName == "menu" && aNode.hidden != true && aNode.disabled != true) {
            // we just want not disabled/hidden menus
            return 4; //conditional accept - accept node, but don't go further down
          } else {
            return controller.window.NodeFilter.FILTER_REJECT;
          }
        } else {
          return controller.window.NodeFilter.FILTER_SKIP;
        }
      }
    }
  },
  
  pressAccesskey : function l10n_pressAccesskey(key) {
    return controller.keypress(null, key, {ctrlKey: mozmill.isMac, altKey: !mozmill.isMac});
  },
  
  validateAccesskeyAction : function l10n_validateAccesskeyAction(senderNode) {
    var activeMenus = controller.window.document.getElementsByAttribute('_moz-menuactive', 'true');
    
    if (activeMenus.length == 0) {
      return true;
    }
    
    for (i = 0; i < activeMenus.length; i++) {
      if (senderNode.id == activeMenus[i].id) {
        return false;
      }
      if (senderNode.nodeName == "menuitem" && activeMenus[i].nodeName == "menuitem" && activeMenus[i].hidden != true && activeMenus[i].disabled != true) {
        return false;
      }
    }
    
    for (i = 0; i < activeMenus.length; i++) {
      if (senderNode.nodeName == "menu" && activeMenus[i].nodeName == "menu" && activeMenus[i].hidden != true && activeMenus[i].disabled != true && activeMenus[i].open && activeMenus[i].open == true) {
        if (senderNode.id.length > 0 && senderNode.id == activeMenus[i].parentContainer.id) {
          return true;
        } else {
          return false;
        }
      }
    }
    
    return true;
  },
  
  abortAccessKeyCheck : function l10n_abortAccesskeyCheck(controller, action, vars) {
    if (action) {
      if (typeof action == "function") {
        action(vars);
      }
      if (action == "closewindow") {
        //try to close an oppened window
        controller.keypress(null, "w", {accelKey: true}); // first try this to prevent closing the main window
        controller.keypress(null, "VK_ESCAPE", {});
      }
    }
    
  },
  
  abortAccessKeyCheckInModal : function l10n_abortAccesskeyCheckInModal(controller) {
    controller.keypress(null, "w", {accelKey: true}); // first try this to prevent closing the main window
    controller.keypress(null, "VK_ESCAPE", {});
  },
  
  checkAccesskeys : function l10n_checkAccesskeys(node) {
    // use this as a callback method for menu-nodes
    
    //var md = new ModalDialogAPI.modalDialog(this.abortAccessKeyCheckInModal);
    //md.start(100);
    
    
    this.l10n.pressAccesskey(node.accessKey);
    controller.sleep(2000);
    // 1st: check if the menupopup was opened
    if (this.l10n.validateAccesskeyAction(node) != true) {
      controller.window.dump("Accesskey '"+node.accessKey+"' for id '"+node.id+"' didn't work");
    } else {
      controller.window.dump("Accesskey '"+node.accessKey+"' for id '"+node.id+"' did work");
    }
    // 2nd: go through all the menu-node children
    if (node.hasChildNodes()) {
      var popupnode = node.childNodes[0];
      if (popupnode.hasChildNodes()) {
        var popupnodes = popupnode.childNodes;
        for (var i = 0; i < popupnodes.length; i++) {
          if (popupnodes[i].nodeName == 'menuitem' && popupnodes[i].accessKey) {
            this.l10n.pressAccesskey(popupnodes[i].accessKey);
            controller.sleep(2000);
            if (this.l10n.validateAccesskeyAction(node) != true) {
              controller.window.dump("Accesskey '"+popupnodes[i].accessKey+"' for id '"+popupnodes[i].id+"' didn't work");
            } else {
              controller.window.dump("Accesskey '"+popupnodes[i].accessKey+"' for id '"+popupnodes[i].id+"' did work");
              controller.sleep(1000);
              controller.keypress(null, "VK_ESCAPE", {});
              this.l10n.pressAccesskey(node.accessKey);
              controller.sleep(2000);
            }
          }
          
        }
      }
      
    }
    // 3rd: try all menuitems containing access keys
      //var md = new ModalDialogAPI.modalDialog(abortAccessKeyCheck);  
      //md.start(100);  
    // 4rd: recursively do this for all sub-menus
  },
  
  // action dict: object like {id : action, id2 : action2}
  actionDict : {},
  
  checkAccesskeysMain : function l10n_checkAccesskeysMain(controller, doc, root, actionDict) {
    this.actionDict = actionDict;
    domWalker.getNodes_recursion_main(doc, root, this.filterNodesAKeysMenus, this.checkAccesskeys, this.blank);
  },
  
  blank : function l10n_blank(blob) {
    return null;
  }
  
}
